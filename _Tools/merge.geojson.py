# USAGE: merge.geojson.py [path] [prefix]
from json import load, JSONEncoder
from os import walk
from re import compile
import sys

float_pat = compile(r'^-?\d+\.\d+(e-?\d+)?$')
charfloat_pat = compile(r'^[\[,\,]-?\d+\.\d+(e-?\d+)?$')

path = sys.argv[1]
if path[-1] != '/':
    path += '/'
prefix = sys.argv[2].lower()

if __name__ == '__main__':
    outjson = dict(type='FeatureCollection', features=[])
    f = []
    infiles = []

    for (dirpath, dirnames, filenames) in walk(path):
        f.extend(filenames)
        break

    for file in filenames:
        if file.split('-')[0].lower() == prefix:
            infiles.append(path+file)

    for infile in infiles:
        postjson = {}
        id = infile.split('-')[-1].split('.')[0]
        injson = load(open(infile))
        postjson['type'] = 'Feature'
        postjson['geometry'] = injson
        postjson['properties'] = {
            "id":id
        }
        outjson['features'].append(postjson)
    
    encoder = JSONEncoder(separators=(',', ':'))
    encoded = encoder.iterencode(outjson)
     
    format = '%.6f'
    output = open(path+prefix+'-merged.json', 'w')
     
    for token in encoded:
        if charfloat_pat.match(token):
            # in python 2.7, we see a character followed by a float literal
            output.write(token[0] + format % float(token[1:]))
 
        elif float_pat.match(token):
            # in python 2.6, we see a simple float literal
            output.write(format % float(token))
 
        else:
            output.write(token)