from requests import get
from pprint import pprint

GAPIS = 'http://maps.googleapis.com/maps/api/geocode/json?address=%s&sensor=false'

class MAPIS:
	point = 'http://global.mapit.mysociety.org/point/4326/%s'
	areas = 'http://global.mapit.mysociety.org/areas/%s'
	coverlaps = 'http://global.mapit.mysociety.org/area/%s/coverlaps'
	geojson = 'http://global.mapit.mysociety.org/area/%s.geojson'

def getAreas(loc):
	json = get(GAPIS % loc).json()['results'][0]['geometry']['location']
	coords = str(json['lng'])+','+str(json['lat'])
	#return coords
	print MAPIS.point % coords + '.html'
	a = raw_input('Area: ')
	for each in get(MAPIS.coverlaps % a).json():
		with open(loc+'-'+each+'.geojson','wb') as handle:
				print 'Downloading '+loc+'-'+each+'.geojson'
				r = get(MAPIS.geojson % each)
				for block in r.iter_content(1024):
					if not block:
						break
					handle.write(block)
loc = raw_input('Location: ')
getAreas(loc)